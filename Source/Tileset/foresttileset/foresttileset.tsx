<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.0" name="foresttileset" tilewidth="128" tileheight="128" tilecount="20" columns="5">
 <image source="png/Tiles/spritesheet.png" width="640" height="513"/>
 <tile id="0">
  <objectgroup draworder="index">
   <object id="2" x="3" y="2">
    <polygon points="0,0 0,124 124,125 122,-4"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="1">
  <objectgroup draworder="index">
   <object id="2" x="1" y="0">
    <polygon points="0,0 0,128 127,126 126,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="2">
  <objectgroup draworder="index">
   <object id="1" x="2" y="1">
    <polygon points="0,0 124,1 128,126 1,128"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="3">
  <objectgroup draworder="index">
   <object id="1" x="1" y="2">
    <polygon points="0,0 10,122 125,122 127,-3"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="4">
  <objectgroup draworder="index">
   <object id="1" x="1" y="1">
    <polygon points="0,0 126,0 126,128 0,126"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="5">
  <objectgroup draworder="index">
   <object id="1" x="0" y="1">
    <polygon points="0,0 1,29 18,51 39,52 50,59 59,70 92,90 114,91 127,92 126,1"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="6">
  <objectgroup draworder="index">
   <object id="1" x="2" y="2">
    <polygon points="0,0 -3,90 19,89 31,84 43,86 59,89 70,90 125,90 126,-2"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="7">
  <objectgroup draworder="index">
   <object id="1" x="124" y="-2">
    <polygon points="0,0 4,45 -121,93 -123,4"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="8">
  <objectgroup draworder="index">
   <object id="1" x="127" y="1">
    <polygon points="0,0 -3,125 -125,126 -126,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="9">
  <objectgroup draworder="index">
   <object id="1" x="1" y="2">
    <polygon points="0,0 0,123 125,125 129,0"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="12">
  <objectgroup draworder="index">
   <object id="1" x="3" y="3">
    <polygon points="0,0 124,0 64,11"/>
   </object>
   <object id="2" x="3" y="2">
    <polygon points="0,0 -3,126 124,125 124,-3"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="13">
  <objectgroup draworder="index">
   <object id="1" x="124" y="2">
    <polygon points="0,0 5,124 -125,124 -123,-2"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="15">
  <objectgroup draworder="index">
   <object id="1" x="1" y="2">
    <polygon points="0,0 2,125 128,126 126,-2"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="17">
  <objectgroup draworder="index">
   <object id="1" x="2" y="1">
    <polygon points="0,0 124,-2 126,128 -1,127"/>
   </object>
  </objectgroup>
 </tile>
 <tile id="18">
  <objectgroup draworder="index">
   <object id="1" x="1" y="2">
    <polygon points="0,0 0,124 125,124 125,-2"/>
   </object>
  </objectgroup>
 </tile>
</tileset>
